package com.webcontrol.virtualkeyboardexample

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.webcontrol.virtualkeyboard.KeyboardActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener {
            startActivityForResult(Intent(this, KeyboardActivity::class.java), KEYBOARD_REQUEST)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == KEYBOARD_REQUEST) {
            if (resultCode == RESULT_OK) {
                val text = data!!.getStringExtra(TEXT)
                Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
            }
        }
    }

    companion object {
        private const val KEYBOARD_REQUEST = 1
        const val TEXT = "TEXT_CONTENT"
    }
}
