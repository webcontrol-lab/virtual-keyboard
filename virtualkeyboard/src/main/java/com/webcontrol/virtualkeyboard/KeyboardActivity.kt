package com.webcontrol.virtualkeyboard

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import android.util.SparseArray
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.activity_keyboard.*
import java.util.*


class KeyboardActivity : KeyboardBaseActivity(), View.OnClickListener {

    private var keyValues: SparseArray<String> = SparseArray()
    private var capsFlag: Boolean = false
    private var buttonsList: ArrayList<MaterialButton> = ArrayList()
    private var ic: InputConnection? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_keyboard)

        ic = txtInput.onCreateInputConnection(EditorInfo())!!

        //prevent system keyboard appear
        txtInput.inputType = InputType.TYPE_NULL
        txtInput.inputType = EditorInfo.TYPE_NULL
        txtInput.setRawInputType(InputType.TYPE_CLASS_TEXT)
        txtInput.setTextIsSelectable(true)

        fillButtonList()
        setButtonListeners()
        setVisualCaps()
        setKeyValues()
    }

    override fun onClick(v: View) {

        if (ic == null) return

        if (v.id == btnBackspace.id) {

            val selectedText: CharSequence? = ic!!.getSelectedText(0)

            if (TextUtils.isEmpty(selectedText))
                ic!!.deleteSurroundingText(1, 0)
            else
                ic!!.commitText("", 1)
        } else if (v.id == btnCapslock.id) {
            toggleCaps()
        } else if (v.id == btnOK.id) {
            val data = Intent()
            data.putExtra(TEXT, txtInput.text.toString())
            setResult(RESULT_OK, data)
            finish()
        } else if (v.id == btnClose.id) {
            finish()
        } else {
            val value = keyValues.get(v.id)
            ic!!.commitText(value, 1)
        }
    }

    private fun fillButtonList() {

        buttonsList.add(btnOne)
        buttonsList.add(btnTwo)
        buttonsList.add(btnThree)
        buttonsList.add(btnFour)
        buttonsList.add(btnFive)
        buttonsList.add(btnSix)
        buttonsList.add(btnSeven)
        buttonsList.add(btnEight)
        buttonsList.add(btnNine)
        buttonsList.add(btnZero)

        buttonsList.add(btnQ)
        buttonsList.add(btnW)
        buttonsList.add(btnE)
        buttonsList.add(btnR)
        buttonsList.add(btnT)
        buttonsList.add(btnY)
        buttonsList.add(btnU)
        buttonsList.add(btnI)
        buttonsList.add(btnO)
        buttonsList.add(btnP)
        buttonsList.add(btnA)
        buttonsList.add(btnS)
        buttonsList.add(btnD)
        buttonsList.add(btnF)
        buttonsList.add(btnG)
        buttonsList.add(btnH)
        buttonsList.add(btnJ)
        buttonsList.add(btnK)
        buttonsList.add(btnL)
        buttonsList.add(btnÑ)
        buttonsList.add(btnZ)
        buttonsList.add(btnX)
        buttonsList.add(btnC)
        buttonsList.add(btnV)
        buttonsList.add(btnB)
        buttonsList.add(btnN)
        buttonsList.add(btnM)
        buttonsList.add(btnSpace)
    }

    private fun setButtonListeners() {
        for (btn in buttonsList) {
            btn.setOnClickListener(this)
        }

        btnBackspace.setOnClickListener(this)
        btnOK.setOnClickListener(this)
        btnCapslock.setOnClickListener(this)
        btnClose.setOnClickListener(this)
        btnCapslock.setOnClickListener(this)
    }

    private fun toggleCaps() {

        capsFlag = !capsFlag

        // visualmente
        setVisualCaps()

        // internamente
        setKeyValues()
    }

    private fun setVisualCaps() {
        for (btn in buttonsList) {
            btn.setSupportAllCaps(capsFlag)
        }
    }

    private fun setKeyValues() {

        keyValues.clear()

        // numbers and letters buttons
        for (btn in buttonsList) {
            keyValues.put(btn.id, checkCaps(btn.text.toString()))
        }
    }

    private fun checkCaps(letter: String): String {

        var result = ""

        if (letter.isNotEmpty()) {
            result = if (capsFlag) letter.toUpperCase(Locale.US) else letter.toLowerCase(Locale.US)
        }

        return result
    }

    companion object {
        const val TEXT = "TEXT_CONTENT"
    }
}
