# VirtualKeyboard

VirtualKeyboard es un teclado virtual que se lanza como actividad fullscreen.

# Tabla de Contenidos
<!-- TOC -->
  
- [Primeros pasos](#primeros-pasos)
- [Proyecto de Ejemplo](#proyecto-de-ejemplo)
- [Licencia](#licencia)


<!-- /TOC -->

## Primeros pasos

Siga los siguientes pasos para usar correctamente la librería.

#### Paso 1: Añade la librería a las dependencias de tu proyecto

Para añadir la librería, necesitas agregar el siguiente código a tu archivo 'build.gradle(app)':

###### build.gradle (module: App)
```gradle
dependencies {
    ...
    implementation 'com.webcontrol:wcvirtualkeyboard:1.0.1'
}
```

### Paso 2: Añade variables estáticas

En la actividad o fragment donde deseas usar el teclado virtual, añade las siguientes variables estáticas:

MyActivity.kt
```kotlin
companion object {
        private const val KEYBOARD_REQUEST = 1
        const val TEXT = "TEXT_CONTENT"
    }
```

MyActivity.java
```java
private static final int KEYBOARD_REQUEST = 1;
private static final String TEXT = "TEXT_CONTENT";
```

Estas variables se usarán en el paso 3 y 4 para llamar a la actividad del teclado virtual.

### Paso 3: Llama al teclado virtual

Para llamar a la actividad que contiene el teclado virtual, usa el método startActivityForResult

MyActivity.kt
```kotlin
startActivityForResult(Intent(context, KeyboardActivity::class.java), KEYBOARD_REQUEST)
```

MyActivity.java
```java
startActivityForResult(new Intent(context, KeyboardActivity.class), KEYBOARD_REQUEST)
```

Asegúrate de haber importado la librería correcta.

MyActivity.kt o MyActivity.java
 
```
import com.webcontrol.virtualkeyboard.KeyboardActivity
```

### Paso 4: Implementa el método onActivityResult 

Aqui recibirás el texto insertado por el teclado virtual

MyActivity.kt
```kotlin
override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == KEYBOARD_REQUEST) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                val textCode : String? = data!!.getStringExtra(TEXT)

                // textCode es el texto recibido al presionar el botón Enviar
            }
        }
    }
```

MyActivity.java
```java
@Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == KEYBOARD_REQUEST) {
            if (resultCode == RESULT_OK) {
                String textCode = data.getStringExtra(TEXT);
            }
        }
    }
```

## Proyecto de Ejemplo

Como ejemplo, en este repositorio, esta incluido un proyecto que implementa la libreria VirtualKeyboard.
Para usarlo solo debes descargar o clonar el repositorio en un proyecto nuevo de Android Studio.

## Licencia

La librería está disponible bajo los términos de [The MIT Licence](https://opensource.org/licenses/MIT)